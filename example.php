<?php
/**
 * User: Sebastian Procek <s.procek@inis.pl>
 * Date: 20.06.17
 * Time: 13:27
 */

use Inis\ExpectusClient;

include __DIR__ . '/src/ExpectusImporter.php';
include __DIR__ . '/vendor/autoload.php';
$importer = new \Inis\ExpectusImporter('APIKEY');

$importer->setCallback('https://requestb.in/ybf7dryb');
$importer->setImportDeduplication(true);
$importer->setCsvDeduplication(false);
$importer->setLabel('jakas-labelka-w-formie-sluga');
$importer->setDelimiter(';');
$importer->setEnclosure('"');
$importer->setCsvFilePathToSend(realpath('test.csv'));
$importer->setMap(
    [
        ExpectusClient::TEST_EMAIL
    ]
);

$response = $importer->import();

var_dump($response);
