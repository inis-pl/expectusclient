<?php
/**
 * Created by PhpStorm.
 * User: Sebastian
 * Date: 2015-07-03
 * Time: 13:04
 */

namespace Inis;

use Curl\Curl;

/**
 * Class ExpectusClient
 *
 * @package ExpectusClient
 */
class ExpectusClient
{
    CONST RESULT_JSON = 'json';
    CONST RESULT_SERIALIZE = 'serialize';
    CONST RESULT_PLAIN = 'plain';

    CONST TEST_IGNORE = '';
    CONST TEST_EMAIL = 'email';
    CONST TEST_GSM = 'gsm';
    CONST TEST_GSM_HLR = 'gsm_hlr';
    CONST TEST_ZIP = 'zip';
    CONST TEST_CITY = 'city';
    CONST TEST_ANSWER = 'answer';
    CONST TEST_FIRSTNAME = 'firstname';
    CONST TEST_LASTNAME = 'lastname';
    CONST TEST_NIP = 'nip';
    CONST TEST_PESEL = 'pesel';
    CONST TEST_REGON = 'regon';

    CONST RECORD_TEST_AVERAGE_POINTS = 'AveragePointsRecord';
    CONST RECORD_TEST_IS_PERFECT = 'IsPerfectRecord';
    CONST RECORD_TEST_ZIP_CITY = 'ZipCityRecord';
    CONST RECORD_TEST_FULLNAME = 'FullnameRecord';

    private $apiUrl = 'https://www.expectus.pl/post';

    private $apiKey;
    private $resultFormat;
    private $hlr = false;
    private $recordTests = [];
    private $records = [];
    private $allResultFormats;
    private $allTests;
    private $gsmCallback;

    /**
     * @param $apiKey
     */
    public function __construct($apiKey)
    {

        $this->apiKey = $apiKey;
        $this->resultFormat = self::RESULT_JSON;
        $this->allResultFormats = [self::RESULT_JSON, self::RESULT_PLAIN, self::RESULT_SERIALIZE];
        $this->allTests = [
            self::TEST_EMAIL,
            self::TEST_GSM,
            self::TEST_GSM_HLR,
            self::TEST_ZIP,
            self::TEST_CITY,
            self::TEST_ANSWER,
            self::TEST_FIRSTNAME,
            self::TEST_LASTNAME,
            self::TEST_NIP,
            self::TEST_PESEL,
            self::TEST_REGON
        ];
        $this->allRecordTests = [
            self::RECORD_TEST_AVERAGE_POINTS,
            self::RECORD_TEST_FULLNAME,
            self::RECORD_TEST_IS_PERFECT,
            self::RECORD_TEST_ZIP_CITY
        ];
    }

    /**
     * @return null
     * @throws \Exception
     */
    public function callAPI()
    {

        $curl = new Curl();

        $params = [
            'result'              => $this->resultFormat,
            'apiKey'              => $this->apiKey,
            'importDeduplication' => false,
            'recordTests'         => $this->recordTests,
            'records'             => $this->records,
            'raport'              => 'extended'
        ];

        if ($this->hlr) {
            $params['gsm_callback'] = $this->gsmCallback;
        }

        $curl->post($this->apiUrl, $params);

        if (json_decode($curl->response, true) !== null) {
            return $curl->response;
        } else {
            throw new \Exception('Response decode error');
        }
    }

    /**
     * @param $format
     *
     * @throws \Exception
     */
    public function setResultFormat($format)
    {
        if (in_array($format, $this->allResultFormats)) {
            $this->resultFormat = $format;
        } else {
            throw new \Exception(
                'unknown format: ' . $format . ', choose from: ' . implode(', ', $this->allResultFormats)
            );
        }
    }

    /**
     * @param $recordTest
     *
     * @throws \Exception
     */
    public function turnOnRecordTest($recordTest)
    {
        if (in_array($recordTest, $this->allRecordTests)) {
            if (!in_array($recordTest, $this->recordTests)) {
                $this->recordTests[] = $recordTest;
            }
        } else {
            throw new \Exception(
                'unknown record test: ' . $recordTest . ', choose from: ' . implode(', ', $this->allRecordTests)
            );
        }
    }

    /**
     * @param $recordTest
     *
     * @throws \Exception
     */
    public function turnOffRecordTest($recordTest)
    {
        if (in_array($recordTest, $this->allRecordTests)) {
            if (($key = array_search($recordTest, $this->recordTests)) !== false) {
                unset($this->recordTests[$key]);
            }
        } else {
            throw new \Exception(
                'unknown record test: ' . $recordTest . ', choose from: ' . implode(', ', $this->allRecordTests)
            );
        }
    }

    /**
     * @param $data
     */
    public function addRecord($data)
    {
        $record = [];
        foreach ($this->allTests as $test) {
            if (isset($data[$test])) {
                $record[$test] = $data[$test];
            }
        }
        $this->records[] = $record;
    }

    /**
     *
     */
    public function clearRecords()
    {
        $this->records = [];
    }

    /**
     *
     */
    public function turnOnHlrMode($callbackUrl)
    {
        $this->gsmCallback = $callbackUrl;
        $this->hlr = true;
    }

    /**
     *
     */
    public function turnOffHlrMode()
    {
        $this->hlr = false;
    }
}