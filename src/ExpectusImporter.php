<?php
/**
 * User: Sebastian Procek <s.procek@inis.pl>
 * Date: 20.06.17
 * Time: 13:10
 */

namespace Inis;

use Curl\Curl;
use CURLFile;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Post\PostFile;

class ExpectusImporter
{
    const EXPECTUS_IMPORT_ENDPOINT = 'https://www.expectus.pl/api/import';

    private $apiKey;
    private $csvFilePathToSend;
    private $delimiter;
    private $enclosure;
    private $importDeduplication;
    private $csvDeduplication;
    private $label;
    private $map;
    private $callback;

    /**
     * @return Curl|string
     */
    public function import()
    {

        $body = [
            'apiKey'              => $this->apiKey,
            'delimiter'           => $this->delimiter,
            'enclosure'           => $this->enclosure,
            'importDeduplication' => $this->importDeduplication,
            'csvDeduplication'    => $this->importDeduplication,
            'label'               => $this->label,
            'map'                 => http_build_query($this->map),
            'callback'            => $this->callback,
            'importFile'          => new CurlFile($this->csvFilePathToSend),
        ];

        $curl = new Curl();
        try {
            $response = $curl->post(self::EXPECTUS_IMPORT_ENDPOINT, $body);
            return $response;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * ExpectusImporter constructor.
     *
     * @param $apiKey
     */
    public function __construct($apiKey)
    {
        $this->apiKey = $apiKey;
    }

    /**
     * @return mixed
     */
    public function getCsvFilePathToSend()
    {
        return $this->csvFilePathToSend;
    }

    /**
     * @param mixed $csvFilePathToSend
     */
    public function setCsvFilePathToSend($csvFilePathToSend)
    {
        $this->csvFilePathToSend = $csvFilePathToSend;
    }

    /**
     * @return mixed
     */
    public function getDelimiter()
    {
        return $this->delimiter;
    }

    /**
     * @param mixed $delimiter
     */
    public function setDelimiter($delimiter)
    {
        $allowed_delimiter = array(';', ',', '\t');
        if (in_array($delimiter, $allowed_delimiter)) {
            $this->delimiter = $delimiter;
        } else {
            $this->delimiter = $allowed_delimiter[0];
        }
    }

    /**
     * @return mixed
     */
    public function getEnclosure()
    {
        return $this->enclosure;
    }

    /**
     * @param mixed $enclosure
     */
    public function setEnclosure($enclosure)
    {
        $allowed_enclosure = array('"', "'");
        if (in_array($enclosure, $allowed_enclosure)) {
            $this->enclosure = $enclosure;
        } else {
            $this->enclosure = $allowed_enclosure[0];
        }

    }

    /**
     * @return mixed
     */
    public function getImportDeduplication()
    {
        return $this->importDeduplication;
    }

    /**
     * @param mixed $importDeduplication
     */
    public function setImportDeduplication($importDeduplication)
    {
        $this->importDeduplication = $importDeduplication;
    }

    /**
     * @return mixed
     */
    public function getCsvDeduplication()
    {
        return $this->csvDeduplication;
    }

    /**
     * @param mixed $csvDeduplication
     */
    public function setCsvDeduplication($csvDeduplication)
    {
        $this->csvDeduplication = $csvDeduplication;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return mixed
     */
    public function getMap()
    {
        return $this->map;
    }

    /**
     * @param mixed $map
     */
    public function setMap($map)
    {
        $this->map = $map;
    }

    /**
     * @return mixed
     */
    public function getCallback()
    {
        return $this->callback;
    }

    /**
     * @param mixed $callback
     */
    public function setCallback($callback)
    {
        $this->callback = $callback;
    }

}
