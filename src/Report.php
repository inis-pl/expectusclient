<?php
/**
 * User: Sebastian Procek <s.procek@inis.pl>
 * Date: 22.06.17
 * Time: 10:16
 */

namespace Inis;

class Report
{
    const STATUS_IN_PROGRESS = 1;
    const STATUS_DONE = 2;
    const STATUS_DELETED = 3;
}