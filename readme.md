# First Step

1) copy skeleton.composer.json to composer.json

2) get composer
```
php -r "readfile('https://getcomposer.org/installer');" | php
```
3) install
```
php composer.phar install
```


# Use Case


```php
<?php

include 'vendor\autoload.php';
use ExpectusClient\ExpectusClient;

$ex = new ExpectusClient('***************API*KEY*************');
$ex->turnOnRecordTest(ExpectusClient::RECORD_TEST_IS_PERFECT);
$ex->turnOnRecordTest(ExpectusClient::RECORD_TEST_FULLNAME);
$ex->turnOnRecordTest(ExpectusClient::RECORD_TEST_ZIP_CITY);
$ex->turnOnRecordTest(ExpectusClient::RECORD_TEST_AVERAGE_POINTS);
$ex->turnOnHlrMode('http://expectus.com.pl/fakeClientHlrCallback.php');
$ex->addRecord([
                   'email' =>'adam@kowalski.pl',
                   'firstname' => 'Adam',
                   'lastname' => 'Kowalski'
                   
               ]);

$result = $ex->callAPI();

var_dump($result);
```